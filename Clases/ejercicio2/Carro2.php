<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $circula; //Variable donde se almcena la verificación

	//declaracion del método verificación
	public function verificacion($fabricacion){
		if ($fabricacion < "1990-00") {			//Antes de 1990
			$this->circula = "no";
		} elseif ($fabricacion > "2010-00") {	//Despues de 2010
			$this->circula = "si";
		} else {								//Entre 1990 y 2010
			$this->circula = "Revisión";
		}
	}

	//Función para obtener el valor que contenga $circula
	public function getCircula(){
		return $this->circula;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	
	//Se hace la verificación con el valor obtenido
	$Carro1->verificacion($_POST['fabricacion']);
}




