<?php
    include_once('transporte.php');

    class moto extends transporte{
		private $marca;

		//sobreescritura de constructor
		public function __construct($nom,$vel,$com,$mar){
			parent::__construct($nom,$vel,$com);
			$this->marca=$mar;
		}

		// sobreescritura de metodo
		public function resumenMoto(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Marca:</td>
						<td>'. $this->marca.'</td>				
					</tr>';
			return $mensaje;
		}
	}

?>